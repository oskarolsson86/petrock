package se.experis;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    private PetRock kalle;

    @BeforeAll
    void beforeAll() {
        kalle = new PetRock("Kalle");
    }

    @Test
    void getName() throws Exception {

        assertEquals("Kalle", kalle.getName());

    }

    @Test
    void testUnHappyToStart() throws Exception {

        assertFalse(kalle.isHappy());
    }

    @Test
    void testHappyAfterPLay() {
        kalle.playWithRock();
        assertTrue(kalle.isHappy());
    }


//    @Test
//    void name() {
//        assertThrows(IllegalStateException.class, () -> kalle.getHappyMessage());
//    }

    @Test
    void nameFail() {
        kalle.playWithRock();
        String message = kalle.getHappyMessage();
        assertEquals("I'm happy!", message);

    }

    @Test
    void testGetFavNumber() {
        assertEquals(8, kalle.getFavNumber());
    }

    @Test
    void emptyNameFail() {

        assertThrows(IllegalArgumentException.class, () -> {
            new PetRock("");
        });
    }
}